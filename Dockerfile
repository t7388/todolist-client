FROM node:16-alpine3.11 AS BUILD
WORKDIR /app
COPY . .
RUN npm ci && npm run build

FROM nginx:1.23.1
COPY nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html*
COPY --from=BUILD /app/todolist-client/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]