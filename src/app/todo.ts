export interface Todo {
    id: number;
    description: string;
    dueDate: string;
    isDone: boolean;
}
