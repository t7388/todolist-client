import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Todo } from './todo';

@Injectable({
  providedIn: 'root'
})

export class TodoService {

  private apiServerUrl = environment.apiServerUrl;

  constructor(private http: HttpClient) { }

  public findAllToDos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(`${this.apiServerUrl}/todo/all`);
  }

  public addTodo(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(`${this.apiServerUrl}/todo/add`, todo);
  }

  public updateTodo(todo: Todo): Observable<Todo> {
    return this.http.put<Todo>(`${this.apiServerUrl}/todo/update`, todo);
  }

  public deleteTodo(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/todo/delete/${id}`);
  }
  
}
